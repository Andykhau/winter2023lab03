public class Kettle
{
	public String colour;
	public String brand;
	public int maxLiter;
	
	public void information() {
		System.out.println("You have chosen the colour: " + this.colour);
		System.out.println("You have chosen the brand: " + this.brand);
		System.out.println("You have chosen the size of: " + this.maxLiter);
	}
	
	public void boilTimer(int seconds)
	{
		System.out.println("The water will be ready in:");
		for(int i = seconds; i >= 0; i--)
		{
			System.out.println(i);
		}
		System.out.println("DING!!!");
		System.out.println("The water is READY!");
	}
}