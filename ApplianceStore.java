import java.util.Scanner;
public class ApplianceStore 
{
	public static void main(String [] args)
	{
		Scanner scan = new Scanner(System.in);
		Kettle[] kettle = new Kettle[4];
		
		for (int i = 0; i < 4; i++)
		{
			kettle[i] = new Kettle();
			
			System.out.println("Enter colour of your kettle?");
			kettle[i].colour = scan.next();
			
			System.out.println("Enter the brand for your kettle?");
			kettle[i].brand = scan.next();
			
			System.out.println("Enter how much liter do you need for your kettle?");
			kettle[i].maxLiter = scan.nextInt();
		}
		
		kettle[0].information();
		
		System.out.println("how much water you need right now?");
		System.out.println("1 = 25% full ; 2 = 50% full ; 3 = 75% full ; 4 = 100% full");
		int timer = scan.nextInt();
		int seconds = 0;

		if (timer == 4)
		{
			seconds = 240;
			kettle[0].boilTimer(seconds);
		}
		else if (timer == 3)
		{
			seconds = 180;
			kettle[0].boilTimer(seconds);
		}
		else if (timer == 2)
		{
			seconds = 120;
			kettle[0].boilTimer(seconds);
		}
		else if (timer == 1)
		{
			seconds = 60;
			kettle[0].boilTimer(seconds);
		}
		
	}
}